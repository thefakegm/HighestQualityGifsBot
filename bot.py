from collections import deque
from time import sleep
import logging

import praw

import settings as s

# Create a logger for when things go wrong.
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

handler = logging.FileHandler('bot.log')
handler.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

logger.addHandler(handler)

# init praw and OAuth
r = praw.Reddit(user_agent=s.USER_AGENT,
                client_id=s.APP_KEY,
                client_secret=s.APP_SECRET,
                access_token=s.ACCESS_TOKEN,
                refresh_token=s.REFRESH_TOKEN)


def get_xpost_urls(subreddit_name):
    """ Get the post url from previously xposted submissions
    :param subreddit_name: the subreddit to get the xposts from
    :returns A deque with all the xpost urls
    :rtype deque:
    """

    subreddit = r.subreddit(subreddit_name)
    xposts = subreddit.new(limit=10)
    xpost_urls = deque([], 10)
    for xpost in xposts:
        xpost_urls.append(xpost.url)

    return xpost_urls


def get_hot_submissions(subreddit_name):
    """
    Get the three hottest submissions from a given subreddit.
    :param subreddit_name: Subreddit to get the submissions from
    :return: A list with submissions
    """
    subreddit = r.subreddit(subreddit_name)
    return subreddit.hot(limit=3)


def get_top_submission(submissions):
    """
    Get the hottest submission. Filter out stickied posts
    :param submissions: A list of submissions
    :return: The hottest submission, probably a GifferDylan post
    """
    for submission in submissions:
        if submission.stickied:
            continue
        else:
            return submission


def check_if_not_submitted(xpost_urls, submission):
    """
    Check if the hottest submission has been submitted before.
    :param xpost_urls:
    :param submission:
    :return:
    """
    if submission.url not in xpost_urls:
        xpost_urls.appendleft(submission.id)
        return True


def submit(submission, subreddit):
    """Submit a post to a given subreddit"""
    submission = r.subreddit(subreddit).submit(title=submission.title, url=submission.url)
    return submission


def comment(xpost, submission):
    """Give credit to the original OP"""
    return xpost.reply(s.CREDIT.format(submission.permalink, submission.author))


def message_op(op, subject, message):
    """Message OP to explain what is going on"""
    r.redditor(name=op).message(subject=subject, message=message)


def main_loop():
    # Get the previously xposted posts' urls
    xpost_urls = get_xpost_urls(subreddit_name=s.SUBREDDIT_POST)
    # Get the three hottest submissions. (three because of stickies)
    hqg_submissions = get_hot_submissions(subreddit_name=s.SUBREDDIT_SCAN)
    # Get the actual hottest post.
    top_submission = get_top_submission(submissions=hqg_submissions)

    # If it is not submitted, submit to a given subreddit, give credit, and message to OP.
    if check_if_not_submitted(xpost_urls=xpost_urls, submission=top_submission):
        # submit it to the given subreddit
        xpost = submit(submission=top_submission, subreddit=s.SUBREDDIT_POST)
        # comment on the post giving credit to OP
        xpost_comment = comment(xpost=xpost, submission=top_submission)
        # Sticky that comment
        xpost_comment.mod.distinguish(sticky=True)

if __name__ == '__main__':
    logger.debug("Starting Bot...")
    while True:
        try:
            main_loop()
            logger.debug("Succesfully posted...")
            # Log success
        except Exception as e:
            # if unsuccessful log the error. This could just be reddit being down. So continue the program
            logger.exception(str(e))
        sleep(300)
else:
    logger.debug("lolwat")
